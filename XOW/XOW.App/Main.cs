﻿using System;
using System.Linq;
using System.IO;
using System.Windows.Forms;

namespace XOW.App
{
	public partial class Main : Form
	{
		private readonly string OrigTitle;
		private readonly string DataFolder = @"Data\";
		private string Folder;
		private DirectoryInfo SessionFolder;
		private DateTime VideoCue;
		private int ClientId = 1;

		private const string DateFormat = "yyyyMMMdd-hhmmss";

		public Main()
		{
			InitializeComponent();
			OrigTitle = Text;
			var path = new FileInfo(Application.ExecutablePath).Directory;
			DataFolder = path.FullName.Contains(@"\bin\")
				? Path.Combine(path.Parent.Parent.Parent.Parent.FullName, DataFolder)
				: Path.Combine(path.Parent.FullName, DataFolder);

			Folder = Path.Combine(DataFolder, @"Sessions\");
			if (!File.Exists(Folder)) Directory.CreateDirectory(Folder);

			Text = OrigTitle + " - " + Folder;
			RefreshSessions();
		}

		private void RecordCtl_Click(object sender, EventArgs e)
		{
			StartStopRecord(SessionsMnuCtl.Checked);
		}

		private void RefreshCtl_Click(object sender, EventArgs e)
		{
			RefreshSessions();
		}

		private void RefreshSessions()
		{
			var dir = new DirectoryInfo(Folder);

			SessionsCtl.Items.Clear();
			SessionsCtl.Items.AddRange(dir.GetDirectories());
			if (SessionsCtl.Items.Count == 1) SessionsCtl.SelectedIndex = 0;

			var visitors = new DirectoryInfo(Path.Combine(DataFolder, "Visitors"));
			AllVisitorsCtl.Items.Clear();
			AllVisitorsCtl.Items.AddRange(visitors.GetFiles("*.txt"));
		}

		private void SessionsCtl_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (SessionsCtl.SelectedIndex == -1)
			{
				SessionFolder = null;
				ReadVisits();
				return;
			}

			SessionFolder = (DirectoryInfo)SessionsCtl.Items[SessionsCtl.SelectedIndex];
			var videos = SessionFolder.GetFiles("*.mp4");
			if (videos.Length == 0)
			{
				SetAction("No videos found in session " + SessionFolder.Name);
				return;
			}

			VideoPlayer.URL = videos[0].FullName;
			SetAction(CueCtl.Text = "Loaded Video " + (VideoCue = videos[0].CreationTime).ToString(DateFormat));
			ReadVisits();
		}

		private void AllVisitorsCtl_SelectedIndexChanged(object sender, EventArgs e)
		{
			var file = AllVisitorsCtl.SelectedIndex != -1 ? (FileInfo)AllVisitorsCtl.Items[AllVisitorsCtl.SelectedIndex] : null;
			if (file == null) return;
			file.CopyTo(Path.Combine(SessionFolder.FullName, DateTime.Now.ToString(DateFormat) + "-" + file.Name), true);
			ReadVisits();
		}

		private void ReadVisits()
		{
			VisitorsCtl.Items.Clear();

			if (SessionFolder == null)
				return;

			VisitorsCtl.Items.AddRange(SessionFolder.GetFiles("*.txt"));

			if (VisitorsCtl.Items.Count > 0) VisitorsCtl.SelectedIndex = 0;
		}

		private void VisitorsCtl_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (VisitorsCtl.SelectedIndex == -1)
			{
				VisitorInfo.Text = string.Empty;
				return;
			}

			var visitor = (FileInfo)VisitorsCtl.SelectedItem;
			VisitorInfo.Text = File.ReadAllText(visitor.FullName);
		}

		private void VisitorsCtl_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyData == Keys.Return) OpenVisitorFile();
		}

		private void OpenVisitorFile()
		{
			var visitor = (FileInfo)VisitorsCtl.SelectedItem;
			System.Diagnostics.Process.Start("notepad.exe", visitor.FullName);
		}

		private void VisitorsCtl_MouseDoubleClick(object sender, MouseEventArgs e)
		{
			var visitor = (FileInfo)VisitorsCtl.SelectedItem;
			if (SessionsCtl.SelectedIndex != -1)
			{
				var secs = visitor.CreationTime.Subtract(VideoCue);
				VideoPlayer.Ctlcontrols.currentPosition = secs.TotalSeconds;
			}
		}

		private void InputCtl_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode != Keys.Return) return;

			if (!InputCtl.Text.Contains("-"))
			{
				SetAction("Doesnt contain hyphen");
				InputCtl.Text = string.Empty;
				System.Media.SystemSounds.Asterisk.Play();
				return;
			}

			var code = InputCtl.Text.Split(new char[] { '-' }, 2)[1];

			var items = AllVisitorsCtl.Items.OfType<FileInfo>().ToList();
			var item = items.SingleOrDefault(x => x.Name == code + ".txt");

			if (item == null)
			{
				SetAction(InputCtl.Text + " not found");
				InputCtl.Text = string.Empty;
				System.Media.SystemSounds.Exclamation.Play();
				return;
			}

			SetAction("Logged Visitor " + InputCtl.Text);
			AllVisitorsCtl.SelectedIndex = items.IndexOf(item);
			System.Media.SystemSounds.Beep.Play();
			InputCtl.Text = string.Empty;
		}

		private void SetAction(string what)
		{
			ActionCtl.Text = what + " at " + DateTime.Now.ToString(DateFormat);
		}
	}
}
