﻿using Accord.DirectSound;
using Accord.Video;
using Accord.Video.DirectShow;
using Accord.Video.FFMPEG;
using System.Drawing;

namespace XOW.App
{
	partial class Main
	{
		VideoCaptureDevice source;
        AudioCaptureDevice audioSource;
        FilterInfoCollection videoSources = new FilterInfoCollection(FilterCategory.VideoInputDevice);
		FilterInfoCollection audioSources = new FilterInfoCollection(FilterCategory.AudioInputDevice);

		private delegate void UpdateVideoDelegate(Bitmap b);

		private UpdateVideoDelegate UpdateVideo;

		private void UpdateVideoFromBitmap(Bitmap b)
		{
			RecordingVideoCtl.Image = (Bitmap)b.Clone();
		}

		private void StartStopRecord(bool record)
		{
			if (UpdateVideo == null)
				UpdateVideo = new UpdateVideoDelegate(UpdateVideoFromBitmap);

			if (record)
			{
				var date = System.DateTime.Now.ToString(DateFormat);
				SessionFolder = new System.IO.DirectoryInfo(Folder + date);
				SessionFolder.Create();
				SetupAndStart(string.Format(@"{0}\XOW_Client{1}_{2}.mp4", SessionFolder.FullName, ClientId, date));
				RecordingVideoCtl.Visible = true;
			}
			else
			{
				source.SignalToStop();
				Writer.Flush();
				Writer.Close();
				RecordingVideoCtl.Visible = false;
				RefreshSessions();
			}
		}

		//https://github.com/accord-net/framework/issues/677
		private readonly VideoFileWriter Writer = new VideoFileWriter();

		void SetupAndStart(string output)
		{
			if (videoSources.Count == 0)
			{
				SetAction("No cameras found. Try running the App again");
				return;
			}

            if (audioSources.Count == 0)
            {
                SetAction("No audio devices found. Try running the App again");
                return;
            }

            var cam = videoSources[0].MonikerString;
            var audio = audioSources[0].MonikerString;
            SetAction("Cam started " + cam + " - " + audio);

			source = new VideoCaptureDevice(cam);
            audioSource = new AudioCaptureDevice(FilterInfo.GetPropertyPages(audio)[0]);

			source.NewFrame += Source_NewFrame;
			source.VideoSourceError += Source_VideoSourceError;
			source.PlayingFinished += Source_PlayingFinished;

            audioSource.NewFrame += this.AudioSource_NewFrame;
            audioSource.AudioSourceError += AudioSource_AudioSourceError;

			Writer.Open(output, 800, 600, 30, VideoCodec.MPEG4);

			source.Start();
		}

        private void Source_PlayingFinished(object sender, Accord.Video.ReasonToFinishPlaying reason)
		{
			SetAction("Processing finished");
		}

		private void Source_VideoSourceError(object sender, VideoSourceErrorEventArgs e)
		{
			SetAction(string.Format("Error: {0}", e.Description));
		}

		private void Source_NewFrame(object sender, NewFrameEventArgs e)
		{
			Writer.WriteVideoFrame(e.Frame);
			Invoke(this.UpdateVideo, e.Frame);
		}

        private void AudioSource_AudioSourceError(object sender, Accord.Audio.AudioSourceErrorEventArgs e)
        {
            SetAction(string.Format("Audio Error: {0}", e.Description));
        }

        private void AudioSource_NewFrame(object sender, Accord.Audio.NewFrameEventArgs e)
        {
            Writer.WriteAudioFrame(e.Signal.RawData);
        }

    }
}
