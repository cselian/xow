﻿namespace XOW.App
{
	partial class Main
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
			this.VideoPlayer = new AxWMPLib.AxWindowsMediaPlayer();
			this.VSplitter = new System.Windows.Forms.SplitContainer();
			this.RecordingVideoCtl = new System.Windows.Forms.PictureBox();
			this.MenuCtl = new System.Windows.Forms.MenuStrip();
			this.SessionsMnuCtl = new System.Windows.Forms.ToolStripMenuItem();
			this.RecordCtl = new System.Windows.Forms.ToolStripMenuItem();
			this.RefreshCtl = new System.Windows.Forms.ToolStripMenuItem();
			this.SessionsCtl = new System.Windows.Forms.ToolStripComboBox();
			this.InputCtl = new System.Windows.Forms.ToolStripTextBox();
			this.AllVisitorsCtl = new System.Windows.Forms.ToolStripComboBox();
			this.PaneSplitter = new System.Windows.Forms.SplitContainer();
			this.SearchCtl = new System.Windows.Forms.TextBox();
			this.VisitorsCtl = new System.Windows.Forms.ListBox();
			this.VisitorInfo = new System.Windows.Forms.TextBox();
			this.StatusBarCtl = new System.Windows.Forms.StatusStrip();
			this.CueCtl = new System.Windows.Forms.ToolStripStatusLabel();
			this.ActionCtl = new System.Windows.Forms.ToolStripStatusLabel();
			((System.ComponentModel.ISupportInitialize)(this.VideoPlayer)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.VSplitter)).BeginInit();
			this.VSplitter.Panel1.SuspendLayout();
			this.VSplitter.Panel2.SuspendLayout();
			this.VSplitter.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.RecordingVideoCtl)).BeginInit();
			this.MenuCtl.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.PaneSplitter)).BeginInit();
			this.PaneSplitter.Panel1.SuspendLayout();
			this.PaneSplitter.Panel2.SuspendLayout();
			this.PaneSplitter.SuspendLayout();
			this.StatusBarCtl.SuspendLayout();
			this.SuspendLayout();
			// 
			// VideoPlayer
			// 
			this.VideoPlayer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.VideoPlayer.Enabled = true;
			this.VideoPlayer.Location = new System.Drawing.Point(0, 27);
			this.VideoPlayer.Name = "VideoPlayer";
			this.VideoPlayer.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("VideoPlayer.OcxState")));
			this.VideoPlayer.Size = new System.Drawing.Size(371, 324);
			this.VideoPlayer.TabIndex = 0;
			// 
			// VSplitter
			// 
			this.VSplitter.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.VSplitter.Location = new System.Drawing.Point(12, 12);
			this.VSplitter.Name = "VSplitter";
			// 
			// VSplitter.Panel1
			// 
			this.VSplitter.Panel1.Controls.Add(this.RecordingVideoCtl);
			this.VSplitter.Panel1.Controls.Add(this.VideoPlayer);
			this.VSplitter.Panel1.Controls.Add(this.MenuCtl);
			// 
			// VSplitter.Panel2
			// 
			this.VSplitter.Panel2.Controls.Add(this.PaneSplitter);
			this.VSplitter.Size = new System.Drawing.Size(510, 354);
			this.VSplitter.SplitterDistance = 374;
			this.VSplitter.TabIndex = 1;
			// 
			// RecordingVideoCtl
			// 
			this.RecordingVideoCtl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.RecordingVideoCtl.Location = new System.Drawing.Point(0, 26);
			this.RecordingVideoCtl.Name = "RecordingVideoCtl";
			this.RecordingVideoCtl.Size = new System.Drawing.Size(374, 325);
			this.RecordingVideoCtl.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.RecordingVideoCtl.TabIndex = 2;
			this.RecordingVideoCtl.TabStop = false;
			this.RecordingVideoCtl.Visible = false;
			// 
			// MenuCtl
			// 
			this.MenuCtl.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SessionsMnuCtl,
            this.InputCtl,
            this.AllVisitorsCtl});
			this.MenuCtl.Location = new System.Drawing.Point(0, 0);
			this.MenuCtl.Name = "MenuCtl";
			this.MenuCtl.Size = new System.Drawing.Size(374, 27);
			this.MenuCtl.TabIndex = 1;
			this.MenuCtl.Text = "menuStrip1";
			// 
			// SessionsMnuCtl
			// 
			this.SessionsMnuCtl.CheckOnClick = true;
			this.SessionsMnuCtl.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.RecordCtl,
            this.RefreshCtl,
            this.SessionsCtl});
			this.SessionsMnuCtl.Name = "SessionsMnuCtl";
			this.SessionsMnuCtl.Size = new System.Drawing.Size(63, 23);
			this.SessionsMnuCtl.Text = "&Sessions";
			// 
			// RecordCtl
			// 
			this.RecordCtl.CheckOnClick = true;
			this.RecordCtl.Name = "RecordCtl";
			this.RecordCtl.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.R)));
			this.RecordCtl.Size = new System.Drawing.Size(206, 22);
			this.RecordCtl.Text = "&Record";
			this.RecordCtl.Click += new System.EventHandler(this.RecordCtl_Click);
			// 
			// RefreshCtl
			// 
			this.RefreshCtl.Name = "RefreshCtl";
			this.RefreshCtl.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F5)));
			this.RefreshCtl.Size = new System.Drawing.Size(206, 22);
			this.RefreshCtl.Text = "Refresh &Sessions";
			this.RefreshCtl.Click += new System.EventHandler(this.RefreshCtl_Click);
			// 
			// SessionsCtl
			// 
			this.SessionsCtl.Name = "SessionsCtl";
			this.SessionsCtl.Size = new System.Drawing.Size(121, 23);
			this.SessionsCtl.SelectedIndexChanged += new System.EventHandler(this.SessionsCtl_SelectedIndexChanged);
			// 
			// InputCtl
			// 
			this.InputCtl.Name = "InputCtl";
			this.InputCtl.Size = new System.Drawing.Size(100, 23);
			this.InputCtl.KeyDown += new System.Windows.Forms.KeyEventHandler(this.InputCtl_KeyDown);
			// 
			// AllVisitorsCtl
			// 
			this.AllVisitorsCtl.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.AllVisitorsCtl.Name = "AllVisitorsCtl";
			this.AllVisitorsCtl.Size = new System.Drawing.Size(121, 23);
			this.AllVisitorsCtl.SelectedIndexChanged += new System.EventHandler(this.AllVisitorsCtl_SelectedIndexChanged);
			// 
			// PaneSplitter
			// 
			this.PaneSplitter.Dock = System.Windows.Forms.DockStyle.Fill;
			this.PaneSplitter.Location = new System.Drawing.Point(0, 0);
			this.PaneSplitter.Name = "PaneSplitter";
			this.PaneSplitter.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// PaneSplitter.Panel1
			// 
			this.PaneSplitter.Panel1.Controls.Add(this.SearchCtl);
			this.PaneSplitter.Panel1.Controls.Add(this.VisitorsCtl);
			// 
			// PaneSplitter.Panel2
			// 
			this.PaneSplitter.Panel2.Controls.Add(this.VisitorInfo);
			this.PaneSplitter.Size = new System.Drawing.Size(132, 354);
			this.PaneSplitter.SplitterDistance = 176;
			this.PaneSplitter.TabIndex = 3;
			// 
			// SearchCtl
			// 
			this.SearchCtl.Dock = System.Windows.Forms.DockStyle.Top;
			this.SearchCtl.Location = new System.Drawing.Point(0, 0);
			this.SearchCtl.Name = "SearchCtl";
			this.SearchCtl.Size = new System.Drawing.Size(132, 20);
			this.SearchCtl.TabIndex = 3;
			// 
			// VisitorsCtl
			// 
			this.VisitorsCtl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.VisitorsCtl.FormattingEnabled = true;
			this.VisitorsCtl.Location = new System.Drawing.Point(-1, 26);
			this.VisitorsCtl.Name = "VisitorsCtl";
			this.VisitorsCtl.Size = new System.Drawing.Size(130, 134);
			this.VisitorsCtl.TabIndex = 2;
			this.VisitorsCtl.SelectedIndexChanged += new System.EventHandler(this.VisitorsCtl_SelectedIndexChanged);
			this.VisitorsCtl.KeyDown += new System.Windows.Forms.KeyEventHandler(this.VisitorsCtl_KeyDown);
			this.VisitorsCtl.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.VisitorsCtl_MouseDoubleClick);
			// 
			// VisitorInfo
			// 
			this.VisitorInfo.Dock = System.Windows.Forms.DockStyle.Fill;
			this.VisitorInfo.Location = new System.Drawing.Point(0, 0);
			this.VisitorInfo.Multiline = true;
			this.VisitorInfo.Name = "VisitorInfo";
			this.VisitorInfo.ReadOnly = true;
			this.VisitorInfo.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.VisitorInfo.Size = new System.Drawing.Size(132, 174);
			this.VisitorInfo.TabIndex = 0;
			// 
			// StatusBarCtl
			// 
			this.StatusBarCtl.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CueCtl,
            this.ActionCtl});
			this.StatusBarCtl.Location = new System.Drawing.Point(0, 369);
			this.StatusBarCtl.Name = "StatusBarCtl";
			this.StatusBarCtl.Size = new System.Drawing.Size(534, 22);
			this.StatusBarCtl.TabIndex = 2;
			this.StatusBarCtl.Text = "statusStrip1";
			// 
			// CueCtl
			// 
			this.CueCtl.Name = "CueCtl";
			this.CueCtl.Size = new System.Drawing.Size(70, 17);
			this.CueCtl.Text = "Video Start: ";
			// 
			// ActionCtl
			// 
			this.ActionCtl.Name = "ActionCtl";
			this.ActionCtl.Size = new System.Drawing.Size(86, 17);
			this.ActionCtl.Text = "Action @ Time";
			// 
			// Main
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(534, 391);
			this.Controls.Add(this.StatusBarCtl);
			this.Controls.Add(this.VSplitter);
			this.MainMenuStrip = this.MenuCtl;
			this.MinimumSize = new System.Drawing.Size(500, 430);
			this.Name = "Main";
			this.Text = "[Exhibit On Watch] Session Studio";
			((System.ComponentModel.ISupportInitialize)(this.VideoPlayer)).EndInit();
			this.VSplitter.Panel1.ResumeLayout(false);
			this.VSplitter.Panel1.PerformLayout();
			this.VSplitter.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.VSplitter)).EndInit();
			this.VSplitter.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.RecordingVideoCtl)).EndInit();
			this.MenuCtl.ResumeLayout(false);
			this.MenuCtl.PerformLayout();
			this.PaneSplitter.Panel1.ResumeLayout(false);
			this.PaneSplitter.Panel1.PerformLayout();
			this.PaneSplitter.Panel2.ResumeLayout(false);
			this.PaneSplitter.Panel2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.PaneSplitter)).EndInit();
			this.PaneSplitter.ResumeLayout(false);
			this.StatusBarCtl.ResumeLayout(false);
			this.StatusBarCtl.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private AxWMPLib.AxWindowsMediaPlayer VideoPlayer;
		private System.Windows.Forms.SplitContainer VSplitter;
		private System.Windows.Forms.SplitContainer PaneSplitter;
		private System.Windows.Forms.ListBox VisitorsCtl;
		private System.Windows.Forms.TextBox VisitorInfo;
		private System.Windows.Forms.TextBox SearchCtl;
		private System.Windows.Forms.MenuStrip MenuCtl;
		private System.Windows.Forms.ToolStripTextBox InputCtl;
		private System.Windows.Forms.StatusStrip StatusBarCtl;
		private System.Windows.Forms.ToolStripStatusLabel CueCtl;
		private System.Windows.Forms.ToolStripStatusLabel ActionCtl;
		private System.Windows.Forms.ToolStripMenuItem SessionsMnuCtl;
		private System.Windows.Forms.PictureBox RecordingVideoCtl;
		private System.Windows.Forms.ToolStripMenuItem RecordCtl;
		private System.Windows.Forms.ToolStripMenuItem RefreshCtl;
		private System.Windows.Forms.ToolStripComboBox SessionsCtl;
		private System.Windows.Forms.ToolStripComboBox AllVisitorsCtl;
	}
}

