﻿using System;

namespace XOW.Core.Models
{
    public class Visitor
    {
        public string FullName { get; set; }

        public string Organization { get; set; }

        //[NotMapped]
        public DateTime? VisitedTime { get; set; }

        public string Information { get; set; }
    }
}
